## Vectors

* ordered array
* does not have a location
* length = sqrt(x<sub>0</sub>^2 + x<sub>1</sub>^2 + ... x<sub>n</sub>^2)

#### Vector Addition
* add corresponding components
* assuming two vectors x and y...  
resulting vector = (x<sub>0</sub> + y<sub>0</sub>) + (x<sub>n</sub> + y<sub>n</sub>)...
* if you lay x heel-to-toe with y or lay y heel-to-toe with x, you'll arrive at same vector (parallelogram method)


#### Scaling
* scaling a vector x by a factor of a (alpha) results in a new vector in the same direction as x, but with a length scaled by a factor of alpha
* computing a(alpha)x means each component of x is multiplied by alpha

#### Vector Subtraction
* combination of vector addition and vector scaling

![vector-subtraction](images/vector-subtraction.png)

#### Scaled Vector Addition
* y := ax + y

#### Linear Combinations
* a linear combination of vectors scales the individual vectors and adds them

#### Dot Product / Inner Product
* think SUMPRODUCT function in Excel
* vectors must have same # of components
* alternative notation: dot(x, y) = x transpose y; row-vector multiplied by a column vector

![](images/dot-product.png)

#### Vector Length
* Euclidean length of a vector equals the square root of the sum of the squares of its components
* length also equals the square root of the dot product of the vector times itself
